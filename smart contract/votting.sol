pragma solidity ^0.4.19;

contract Voting {
    
    struct Vote {
        bytes32 question;    
        bytes32[] candidateList;
        mapping (bytes32 => uint8) votesReceived;
    }
    
    Vote[] private votes;
    bytes32 title;
    bytes32 description;

    function setDescription(bytes32 _description) public {
        description = _description;
    }
    
    function getDescription() public view returns(bytes32) {
        return description;
    }

    function setTitle(bytes32 _title) public {
        title = _title;
    }
    
    function getTitle() public view returns(bytes32) {
        return title;
    }

    function createPoll(bytes32 question , bytes32[] candidateNames) public {
        votes.push(Vote(question , candidateNames));
    }
    
    function getSizeVotes() public view returns(uint){
        return votes.length;
    }
    
    function getVote(uint index) public view returns(bytes32 question , bytes32[] candidateList) {
        return (votes[index].question , votes[index].candidateList);
    }

    function totalVotesFor(uint index , bytes32 candidate) view public returns (uint8) {
        return votes[index].votesReceived[candidate];
    }

    function voteForCandidate(uint index , bytes32 candidate) public {
        votes[index].votesReceived[candidate] += 1;
    }
}