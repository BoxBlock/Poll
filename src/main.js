import Vue from 'vue'
import App from './App.vue'
import Contact from './components/Contact.vue'
import Home from './components/Home.vue'
import Question from './components/Question.vue'
import Create from './components/Create.vue'
import View from './components/View.vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)
import webs3 from 'web3';
Vue.use(webs3);
import BootstrapVue from 'bootstrap-vue'
Vue.use(BootstrapVue);
import store from './store'


const routes=[
  {path:"/",component:Home},
  {path:"/create",component:Create},
  {path:"/contact",component:Contact},
  {path:"/view",component:View},
]

const router = new VueRouter({
  mode:'history',
  routes
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
