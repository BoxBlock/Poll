import * as types from './mutation-types'

export const setWeb = ({commit},new_value)=>{
    commit(types.SET_WEB,new_value)
}